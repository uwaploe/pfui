module.exports = {
    publicPath: './',
    indexPath: 'pfu.html',
    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = 'inVADER PFU'
                return args
            })
      }
}
