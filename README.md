# Web-based UI for the inVADER PFU

This repository contains the [Vue.js](https://vuejs.org) based frontend for the Web App used to control and monitor the inVADER PFU board.

## Installation

- Download the latest compressed-tar archive from the Downloads section
- Unpack in the `/var/www/html` directory on the inVADER SIC

## Development

Development uses the [Vue.js](https://cli.vuejs.org/guide/) CLI tools.

### Compiles and hot-reloads for development

``` shellsession
$ npm run serve
```

### Compiles and minifies for production

``` shellsession
$ npm run build
```

### Lints and fixes files

``` shellsession
$ npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Create a new release tarball

``` shellsession
$ ./builddist.sh
```
