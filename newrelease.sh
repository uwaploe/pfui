#!/usr/bin/env bash

VERSION="$1"
[[ -z $VERSION ]] && {
    echo "Usage: ${0##*/} version"
    exit 1
}
tag="v${VERSION}"

set -e
npm run lint

cp package.json package.json.bak
jq -M ".version |= \"$VERSION\"" package.json.bak > package.json
git add package.json
git commit -m "New release"
git tag -a "$tag" -m "Release $tag"
