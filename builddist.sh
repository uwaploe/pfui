#!/usr/bin/env bash

set -e
npm run build

vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   cat $(pwd)/.version 2> /dev/null || echo v0)
VERSION="${vers#v}"
name=$(jq -r -M '.name' package.json)

echo "${name}_${VERSION}"
cd dist
tar -c -v -z -f ../${name}_${VERSION}.tar.gz *
cd -

export VERSION
/usr/local/bin/nfpm pkg --target pfui_${VERSION}_any.deb
